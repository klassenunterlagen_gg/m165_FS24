![tbz_logo](../../x_gitresources/tbz_logo.png)

# Backup und Restore

**Form**: Individuell

**Zeit**: 3 Lektionen

### Auftrag

Sie erarbeiten sich das Wissen über [Backup und Restore selbstständig](https://www.mongodb.com/docs/manual/core/backups/). Sie sollen sich hier **einen Überblick verschaffen** über die vier Backup-Arten und nicht alle anwenden!

Sie erstellen eine Dokumentation mit Screenshots (z.B. Word oder MarkDown) und zeigen so die verschiedenen Zustände und/oder Befehle die sie benötigen. Folgen sie folgendem Leitfaden

### Leitfaden Aufträge

**Aufgabe 1**: Wir beschäftigen uns nun intensiver mit AWS Snapshots - also eine der Backup-Arten wie sie im Link definiert wurden. Beschreiben sie ein Szenario bei welchem sie mit Snapshots von Instanzen (oder Volumen) arbeiten. Bei welcher Firmenumgebung und Infrastruktur würden sie diese Methode empfehlen?

**Aufgabe 2**: Erstellen sie ein Backup und Restore mit Snapshots in der AWS Console und Dokumentieren sie jeweils den Status nach den einzelnen Schritten. Gehen sie - in der AWS Console - zu dem Navigationspunkt **Snapshots** => **Create Snapshot**. Hier spielt es keine Rolle, ob sie ein Snapshot ihres Volumes machen oder von ihrer Instanz. Wenn sie ihre Instanz als Snapshot-Quelle verwenden, würden sie alle Volumes (Disks) einbeziehen. Wenn sie nur ein Snapshot eines Volumes machen, beziehen sie nur gerade diese eine *"Disk"* mit ein. 

Quelle: <https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-creating-snapshot.html>

**Aufgabe 3**: Nun wollen wir Daten wiederherstellen. Um dies zu testen, löschen sie zuerst eine Datenbank aus MongoDB (via GUI oder Shell) oder machen andere Änderungen (z.B. fügen Einträge hinzu). Wenn sie das Volumen wiederhergestellt haben, sollten die Änderungen rückgängig gemacht sein und ihre gelöschte Datenbank ist wieder verfügbar.

Markieren sie einen Snapshot und erstellen sie ein Volumen von diesem Snapshot über **Action** => **Create volume from snapshot**. Wichtig hier ist, dass sie das Volume **in der gleichen** *Availability Zone* erstellen wie ihre Instanz ist. Schauen sie also **zuerst** dort nach. 

Nun müssen sie nacheinander folgende Schritte durchführen:

1. Ihre Instanz stoppen (nicht terminieren sonst ist sie weg)
2. Wechseln sie nun zu dem Navigationspunkt **Volumes**. Dort markieren sie das Volume, welches *in-use* ist. Über **Actions** => **Detach volume**. entfernen sie die alte *"Disk"*. 
3. Markieren sie nun ihr kürzlich erstelltes Volume und fügen sie es der Instanz hinzu **Actions** => **Attach volume**. 
   **WICHTIG**: Unter "Device name" verwenden sie "/dev/sda1". Dies ist die Konvention in Linux für ein bootbares Volume. 

Quelle: <https://docs.aws.amazon.com/prescriptive-guidance/latest/backup-recovery/restore.html>

**Aufgabe 4**: Wir beschäftigen uns nun intensiver mit den Backup- und Restore Befehlen von MongoDB. Beschreiben sie ein Szenario bei welchem sie mit `mongodumb` arbeiten. Bei welcher Firmenumgebung und Infrastruktur würden sie diese Methode empfehlen?

**Aufgabe 5**: Erstellen sie ein Backup und Restore mit `mongodumb` und Dokumentieren sie jeweils den Status nach den einzelnen Schritten

1. Erstellen sie ein Backup mit `mongodumb` ([Doku](https://www.mongodb.com/docs/database-tools/mongodump/?_ga=2.243934022.2145562843.1664790114-171011013.1658772957))
2. Löschen sie eine Datenbank aus MongoDB (via GUI oder Shell)
3. Stellen sie die Datenbank wieder her mit `mongorestore`. ([Doku](https://www.mongodb.com/docs/database-tools/mongorestore/))

------


Letzte Aktualisierung: 10.06.2022, Yves Nussle