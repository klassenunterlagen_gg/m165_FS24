![tbz_logo](../../x_gitresources/tbz_logo.png)

# Benutzer, Rechte und Rollen

**Form**: Individuell

**Zeit**: 3 Lektionen

### Auftrag

Sie erarbeiten sich das Wissen über Authentifizierung und Autorisierung selbstständig. Im [Kurz-Manual](../../MongoDb.md) über MongoDB finden sie ein entsprechendes Kapitel mit kurzen Texten und - **viel wichtiger** - den Links auf die MongoDB Dokumentation.

Sie erstellen eine JavaScript-Datei (**security.js**), welche ihnen als Referenz dient. Schreiben sie dort alle Befehle auf, die sie verwendet haben und Kommentieren sie **ausführlich**. Ein Beispiel kann wie folgt aussehen:

```javascript
// Leitfrage 1: Comment with what you are going to show/do
db.collectionName.doSomething({...});
// Comment with output from MongoDb Shell

// Leitfrage 2: comment with what you are going to show/do
db.collectionName.doSomethingElse({...});
db.collectionName.anotherCommand({...});
// Comment with output from MongoDb Shell

```

In ihrer JavaScript-Datei sollen sie **alle folgenden Leitfragen** beantworten. Verwenden sie für ihre Abfragen die MongoDB Shell (**nicht** via MongoDB Compass). Sie können diese entweder via Ubuntu Shell aufrufen, indem sie sich zuerst [via SSH](https://gitlab.com/ch-tbz-it/Stud/m165/m165_scripts/-/blob/main/ManualAccessVM.md) auf den Server verbinden oder sie laden eine [Version für ihr Betriebssystem herunter](https://www.mongodb.com/try/download/shell)

**Leitfaden**

**Aufgabe 1**: Erklären Sie den Verwendungszweck der folgenden drei Datenbanken in 1-3 Sätzen: admin, local, config. Wie finden sie Informationen dazu?

Loggen sie sich auf den Linux Server [ein mit ssh](https://gitlab.com/ch-tbz-it/Stud/m165/m165_scripts/-/blob/main/ManualAccessVM.md). Sie haben ein Key-Pair erstellt, als sie eine Instanz in AWS hinzugefügt haben. Verwenden sie die .pem-Datei, die sie damals heruntergeladen haben. Mögliche Probleme:

1. Fehlermeldung betreffend Sicherheit: Auf ihre .pem-Datei dürfen nur Sie persönlich und die Administratoren-Gruppe zugriff haben.  Kopieren sie die Datei in den Ordner `C:\Users\<Ihr Benutzer>\.ssh`.
2. Sie verwenden eine falsche .pem-Datei
3. Ihr Pfad auf die Datei ist nicht korrekt

**Aufgabe 2**: Starten sie nun die MongoDB Shell (auf der Linux Shell) mit dem Befehl `mongosh`. Zeigen sie mit einem insert-Befehl, dass sie in der MongoDB Shell die gleichen Befehle verwenden können wie vorher in MongoDB Compass. Sie beweisen, dass sie sich nicht authentifizieren müssen, um Befehle auszuführen.

**Aufgabe 3**: Erstellen sie einen Admin-Benutzer für ihren MongoDB Server. Dieser Benutzer kriegt die Rechte um Benutzer zu administrieren und Lese- und Schreibrechte auf alle Datenbanken.

```javascript
use admin
db.createUser(
  {
    user: "admin",
    pwd: passwordPrompt(), // or cleartext password
    roles: [
      { role: "userAdminAnyDatabase", db: "admin" },
      { role: "readWriteAnyDatabase", db: "admin" }
    ]
  }
)
```

Mögliche Probleme:

- Sie erstellen den Benutzer nicht in der admin-Datenbank, aber werden anschliessend versuchen gegen die admin-Datenbank zu authentifizieren. Stellen sie sicher, dass die korrekte Datenbank verwendet wird.
- Sie vergessen ihr Passwort. Dies passiert öfters als sie erwarten. Für diesen Auftrag, schreiben sie doch ihr Passwort einfach als Cleartext und verwenden sie ein dummy-Passwort.

Aktivieren sie nun die die Sicherheit für MongoDB. Da sie sich immer noch in der MongoDB Shell befinden, müssen sie diese zuerst verlassen mit dem Befehl `exit`. Sie befinden sich nun wieder in der Linux Shell. Sie werden nun die Konfigurationsdatei des MongoDB-Services verändern und MongoDB neu starten. Machen sie sich bewusst, dass sie nun ein Linux-Service verändern und darum in Linux arbeiten.

Die Datei, die sie anpassen müssen ist */etc/mongod.conf*. Dies können sie mit dem Tool *nano* (Erinnern sie sich dazu an das Modul m122). Der Befehl ist `sudo nano /etc/mongod.conf`. Der Zusatz *sudo* ist notwendig, damit sie die Datei mit Administratoren-Rechte ausführen können. 

![nano](./x_ressources/nano.png)

Mögliche Probleme:

- Sie verwenden den Tabulator zum einschieben des Texts. Der MongoDB-Server wird nicht korrekt neu starten
- Sie haben andere Leerzeichen, z.B. vor "security". Diese Datei ist heikel und sie sollten sie nur mit Vorsicht verändern.

Mit ctrl+O speichern sie die Datei und mit ctrl+x schliessen sie nano wieder. Nun müssen sie den MongoDB Service neu starten, damit die Änderungen aktiv werden. Dies tun sie mit dem Befehl `sudo systemctl restart mongod`. Sie überprüfen nun mit dem Befehl `sudo systemctl status mongod`, ob ihr Service korrekt läuft.

Quelle: <https://www.mongodb.com/docs/manual/tutorial/configure-scram-client-authentication/>

**Aufgabe 4**: Starten sie die MongoDB Shell wieder. Zeigen Sie, dass man einen Fehler kriegt, wenn man einen Insert-Befehl ausführt, z.B `db.test.insertOne({"name": "ihr name"})`. Dies kommt daher, dass die Rechte aktiviert wurden, sie sich aber ohne Benutzer in die MongoDB Shell eingeloggt haben. Verlassen sie die MongoDB Shell nun wieder.

Starten sie MongoDB Compass und verbinden sie sich mit ihrem Server. Hier werden sie bereits eine Fehlermeldung kriegen beim Verbindungsaufbau aus dem gleichen Grund (Aktivierung der Authentifizierung).

**Aufgabe 5**:  Starten sie die MongoDB Shell wieder, aber loggen sie sich dabei ein mit dem Admin-Benutzer, welchen sie vorher erstellt hatten. Der Befehl dazu ist immer noch `mongosh`, aber nun mit Parameter: `mongosh --authenticationDatabase "admin" -u "ihr benutzername" -p "ihr passwort"`.  Führen sie nun den gleichen Insert-Befehl aus und zeigen sie, dass er nun funktioniert.

Erstellen sie eine neue Verbindung in MongoDB Compass und verwenden sie einen Connection String mit Benutzername und Passwort. Ein allgemeines Schema sieht so aus: `mongodb://<benutzer>:<password>@<ihr-host>:27017/<Ihre-DB>?authSource=<authentifzierungs-DB>`

z.B.: `mongodb://usrSchool:myPassword@3.83.89.113:27017/intro?authSource=admin`

**Aufgabe 6**:  [Erstellen Sie einen Benutzer](https://www.mongodb.com/docs/manual/tutorial/create-users/#std-label-create-users) (*usrSchool*) für unsere **School**-Datenbank. Dieser Benutzer soll **Lese- und Schreibrechte** besitzen. Befassen sie sich also zuerst mit den [vordefinierten Rollen](https://www.mongodb.com/docs/manual/reference/built-in-roles/). **Achtung**: Seien sie sich bewusst, dass sie Benutzer in verschiedenen Datenbanken anlegen können. Überlegen sie sich gut in welcher Datenbank dieser Benutzer erstellt wird. Begründen sie ihre Entscheidung in einem Kommentar. Als Auswahl steht in diesem Fall, z.B. die `admin`-Datenbank oder die `school`-Datenbank zur Auswahl.

**Aufgabe 7**: Zeigen Sie, dass dieser Benutzer Zugriff auf die School-Datenbank hat mit einer Abfrage, z.b. einem *find* und einem *insertOne*.

**Aufgabe 8**: Zeigen Sie, dass dieser Benutzer keinen Zugriff auf andere Datenbanken hat (z.B. auf eine eigene, andere Test-Datenbank) mit einer Abfrage.

**Aufgabe 9**: Erstellen sie einen neuen Benutzer (*usrSchoolRead*) für unsere **School**-Datenbank. Dieser Benutzer wird **nur lesen** dürfen. Verwenden sie die vordefinierten Rollen. Zeigen Sie mit einem insertOne-Befehl, dass sie tatsächlich nicht schreiben dürfen. Zeigen sie mit einem *find*-Befehl, dass dieser Benutzer lesen darf.

**Aufgabe 10**: Definieren Sie eine [eigene Rolle](https://www.mongodb.com/docs/manual/core/security-user-defined-roles/) (*roleClassRead*), die nur Lese-Rechte auf die Collection *Class* (oder früher *SchoolClass*) in der Datenank *school* besitzt.

**Aufgabe 11**: Weisen Sie dem Benutzer *usrSchoolRead* die Rolle *roleClassRead* zu und zeigen Sie, dass dieser Benutzer nur die Collection *Class* lesen kann mit entsprechenden Abfragen.

**Aufgabe 12**: Löschen sie den Benutzer *usrSchoolRead* und die Rolle *roleClassRead* wieder mit den entsprechenden MongoDB Befehlen.



Letzte Aktualisierung: 06.10.2022, Yves Nussle