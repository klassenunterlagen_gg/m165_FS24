![tbz_logo](./x_gitresources/tbz_logo.png)

# Software

[TOC]

## MongoDB Tools

- MongoDB Shell (CLI)
- MongoDB Compass (GUI)

<https://www.mongodb.com/try/download/tools>

## MariaDB Tools

- MySql Workbench: <https://dev.mysql.com/downloads/workbench/>
- Heidi SQL (GUI): <https://www.heidisql.com/download.php>



Letzte Aktualisierung: 03.08.2022, Yves Nussle