![tbz_logo](../../x_gitresources/tbz_logo.png)

# Erweiterte Abfragen Üben

**Form**: Individuell

**Zeit**: 90 Minuten

**Vorgehen:** 

1. Erstellen sie zuerst **alle** Abfragen im CLI (MongoDB Shell)
2. Dann - falls Zeit bleibt - die Abfragen in der Java Applikation.

### Abfragen

Erstellen sie für jeden Punkt **eine eigene** Abfrage! Arbeiten sie jeweils mit **Aggregationen**!

1. Geben sie alle Schüler zurück, die zu der Klasse 2 gehören. 
2. Erweitern sie Abfrage aus 1. und geben sie nur die Namensfelder zurück.
3. Erweitern sie die Abfrage aus 2. und sortieren sie die Schüler nach Nachname, dann nach Vorname.
4. Erweitern sie die Abfrage aus 3. nun so, dass nur die Klassensprecher zurück gegeben werden, aber fügen sie einen neuen *$match* Stage hinzu. Also erweitern sie nicht die erste. Überlegen sie sich wieso sie kein Resultat zurückbekommen. Es gibt wahrscheinlich zwei Gründe. Sie können zuerst den ersten *$match* Stage erweitern und so testen.
5. Erweitern sie die Abfragen aus 3. so, dass nur diejenigen zurückgegeben werden, welche mit dem Buchstaben "G" beginnen. Sie können den bestehenden *$match* Stage erweitern.
6. Erstellen sie eine Abfrage, die die Studenten mit der *ClassId* gruppieren und die Anzahl Studenten pro *ClassId* zurückliefern. Verwenden sie die Stages *$group* und *$count*. Geben sie ClassId als *_id* zurück.
7. Erstellen sie eine neue Abfrage und verknüpfen sie die  Collections *Student* und *Class*. **Verwenden sie *Student* als Basis** (db.Student.aggregate([])). Sie sehen, dass die Klassen einem Array hinzugefügt wird. Das Verhalten ist daher im Vergleich zu einem SQL-Join sehr unterschiedlich. Wenn sie nun eine Gruppierung vornehmen müssten (wie in 6.), aber den Namen der Klasse als Schlüssel verwenden sollen, wüssten sie wie vorgehen?
8. Verwenden sie $unwind, um die Arrays aufzulösen und erstellen sie die Gruppierung und verwenden den Namen der Klasse als Schlüssel. **Achtung**: Sie kriegen die leeren Klassen mit dieser Version nicht zurück.
   Verwenden sie *$size* in der *$project* Stage (ohne *$group*), um das gleiche Resultat zu erhalten, aber nun kriegen sie auch die leeren Klassen zurück.



------

Letzte Aktualisierung: 02.09.2022, Yves Nussle