![tbz_logo](../../x_gitresources/tbz_logo.png)

# CRUD Befehle Üben

**Form**: Individuell

**Zeit**: 90 Minuten



### Übung 1

**Verwenden sie das GUI (MongoDb Compass)** um folgende Einträge zu erstellen:

- Neue Klasse "AP22b"
- Fügen sie dieser Klasse (mindestens) zwei Schüler hinzu. 

 ### Übung 2

**Verwenden sie die Shell (mongodbsh)** um folgende Einträge hinzuzufügen. **Speichern sie die Befehle** in einer Datei, so dass die Lehrperson diese anschauen kann.

- Einträge welche Lehrperson die Module für die neue Klasse "AP22b" unterrichten.
- Noten für die Schüler der neuen Klasse für jedes Modul.

### Übung 3

Verwenden sie **[das neue verschachtelte Modell](./School_II_ML.json)** für die Demo-Applikation als Referenz. Erstellen sie eine neue Datenbank und setzen sie dieses Model um. Übernehmen sie die Daten (zumindest Teile davon) aus der bestehenden Datenbank (via GUI oder CLI). 

Sie werden zukünftig mit ihrem neuen Modell und der neuen Datenbank arbeiten.

### Übung 4

Erweitern sie die [Java Applikation](./SchoolAdministration.zip), so dass sie nicht nur Klassen anzeigen können, sondern auch

- Auf der **neuen** Datenbank aufbaut.
- Klassen hinzufügen. Das UI dazu wurde bereits erstellt. Versuchen sie (noch) nicht die Id zu berechnen. Dies kommt später.
- Klasse umbenennen. Das Jahr anpassen können sie gerne zusätzlich noch hinzufügen.
- Klasse löschen. Stellen sie hier zuerst sicher, dass sich keine Schüler in der Klasse mehr befinden. **Achtung**: Wenn sie sich das Modell anschauen, sehen sie, dass wir die Klasse nicht wirklich löschen, sondern ein entsprechendes Feld setzen.

------

Letzte Aktualisierung: 02.09.2022, Yves Nussle