![tbz_logo](../../x_gitresources/tbz_logo.png)

# Selbstständiges Erarbeiten der Befehle



Arbeiten sie sich selbstständig in die Befehle von MongoDB ein. Sie werden diese Befehle später weiter vertiefen und üben. Hier geht es darum, dass sie **einen Überblick kriegen** und nicht, dass sie jeden Befehl bereits anwenden können.  

Wenn sie trotzdem die Befehle üben möchten, erstellen sie sich eine **neue** Datenbank. Üben sie **nicht** mit der Demo-App-Datenbank. 

Sie finden einen Einstieg in [diesem Dokument](../../MongoDb.md). Was sollten sie sich anschauen:

- Compass: GUI, um Dokumente grafisch einzufügen. Verständnis für das Verhalten des Feldes *_id* (ca. 10 Minuten) 
- MongoSh: Befehle für Selects, Updates, Deletes (ca. 10 Minuten)
- MongoSh: Logische und Vergleich Operatoren (ca. 10 Minuten)
- MongoSh: Projektionen (ca. 10 Minuten)
- MonboSh: Methoden wie pretty(), max(), limit(), etc (ca 5 Minuten)

**Form**: Individuell

**Zeit**: 30 Minuten

------

Letzte Aktualisierung: 11.08.2022, Yves Nussle