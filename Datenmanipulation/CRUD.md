![tbz_logo](../x_gitresources/tbz_logo.png)

# MongoDB CRUD Operationen

[TOC]

Die **MongoDB-Dokumentation zu CRUD-Operationen**: <https://www.mongodb.com/docs/manual/crud/>

## Grundsätzliches

Der Primärschlüssel von jedem Dokument ist im Wert *_id* gespeichert. Wenn kein entsprechendes Feld im Dokument mitgeliefert wird, wird ein Wert generiert. Es gibt also kein Dokument in MongoDB ohne dem Feld *_id*.

Dert Wert des Feldes *_id* ist ein [Wert des Typs BsonId](https://www.mongodb.com/docs/manual/reference/method/ObjectId/).

**Zusammen mit ihrer Installation der MongoDB, haben sie auch Demo-Daten bekommen. Sie können jederzeit CRUD-Operationen selbstständig testen.**

### Verbindungsaufbau via CLI

Sie haben zwei Möglichkeiten via CLI (Command Line Interface) auf ihre MongoDB Umgebung zu gelangen. 

- Sie [verbinden sich via SSH](https://gitlab.com/ch-tbz-it/Stud/m165/m165_scripts/-/blob/main/ManualAccessVM.md) auf den Linux Server. Dort ist die CLI bereits installiert
- Sie laden das CLI herunter. Den Link kann in [diesem Dokument](../Software.md) gefunden werden. 

Nach der Installation starten sie das Command Prompt (oder starten die Linux Shell) und geben den folgenden Befehl ein.

-  `<Pfad-zu-mongodb-shell>\mongosh`: Dieser Befehl funktioniert, wenn sie bereits auf dem richtig Server sind und keinen ConnectionString mehr angeben müssen.
-  `<Pfad-zu-mongodb-shell>\mongosh "ConnectionString"`. Beispiel: mongosh "mongodb://54.204.116.170:27017/". Diesen Befehl verwenden Sie, wenn Sie, wenn sie von ihrem Computer auf einen entfernten MongoDB Server zugreifen müssen.

### Administration

- `show databases`: Zeigt alle Datenbanken
- `show dbs`: gleich wie `show databases`
- `use <database>`: Wechselt zu der Datenbank
- `show collections`: Zeigt alle Collections
- `show tables`: gleich wie `show collections`

**Datenbank erstellen**: Es benötigt keinen Befehl, um eine Datenbank zu erstellen. Verwenden sie *use datenbankname*, um in die Datenbank zu wechseln. Sobald sie Dokumente hinzufügen, wir die Datenbank erstellt. Es gibt aber Befehle, um explizit leere Datenbanken zu erstellen.

**Collection erstellen**: Es benötigt keinen Befehl, um eine Collection zu erstellen. Sobald sie ein Dokument einer Collection hinzufügen, wird diese erstellt. Es gibt aber Befehle, um explizit leere Collections zu erstellen.

## CRUD Operationen

### Read

- `db.<collection>.find(<json-query>,<json-projection>)`: Findet ein oder mehrere Dokumente und liefert spezifische Inhalte zurück. Wenn sie alle Inhalte zurück liefern möchten, kann der Abfrage-Teil weggelassen werden.
- `db.<collection>.findOne(<json-query>,<json-projection>)`: Findet genau ein (oder das erste) Dokument und liefert spezifische Inhalte zurück. 

In beiden Methoden, können sie die gleichen Parameter hinzufügen.

**Filterung**

Der erste Parameter `<json-query>` ist ein JSON-Dokument, welcher die Daten Filtert. Vergleichbar ist dieser Parameter mit dem *WHERE*-Statement einer SQL-Abfrage.

Folgend ein Beispiel einer einfachen SQL-Abfrage und wie das Äquivalent in einer MongoDB-Abfrage aussehen würde:

~~~sql
SELECT * FROM inventory WHERE status = 'A'
~~~


~~~shell
db.inventory.find({status: 'A'})
~~~

Natürlich sind auch komplexere Filterungen möglich mit den entsprechenden [logischen](https://www.mongodb.com/docs/manual/reference/operator/query-logical/) und [vergleichs-](https://www.mongodb.com/docs/manual/reference/operator/query-comparison/) Operatoren. Dazu ein weiteres Beispiel:

~~~sql
SELECT * FROM inventory WHERE status = 'A' or quantity < 30
~~~


~~~shell
db.inventory.find({$or: [{status: 'A'}, {quantity:{$lt:30}}]})
~~~

**Projektionen**

Wenn sie nur spezifische Felder zurückgeben möchten, können sie dies steuern, indem sie der Methode *find()* und *findOne()* ein zusätzliches Objekt übergeben. Diese Technik nennt sich [*Projection*](https://www.mongodb.com/docs/manual/tutorial/project-fields-from-query-results/) und lässt sich vergleichen mit dem *SELECT*-Statement einer SQL-Abfrage. Dieser zweite Parameter ist optional wie in den Beispielen oben klar geworden ist. 

```sql
SELECT status, quantity FROM inventory
```

~~~shell
db.inventory.find({}, {status:1, _id:0, quantity:1})
~~~

Im Fall oben, werden die beiden Felder *status* und *quantity* zurückgeliefert, aber nicht das Feld *_id*. Dies wird gesteuert mit den Werte 1 (anzeigen) und 0 (nicht anzeigen). Sobald sie eine Projektion übergeben, werden nur die Felder zurückgeliefert, die dort definiert sind. Sie müssen nicht alle anderen Felder ausblenden. Die Ausnahme bildet das Feld *_id*, welches immer zurückgeliefert wird, ausser sie verhindern dies explizit. 

**Beispiel**

Folgend ein Beispiel, welches sowohl Filterung als auch Projektion verwendet

~~~sql
SELECT _id, status,quantity FROM inventory WHERE status = 'A' and quantity = 30
~~~


~~~shell
db.inventory.find({$and: [{status: 'A'}, {quantity:{$eq:30}}]}, {status:1, quantity:1})
~~~

### Insert

- `db.<collection>.insertOne(<insert-json>)`: Fügt ein Dokument der Collection hinzu. Der Parameter muss ein Objekt sein.
- `db.<collection>.insertMany(<insert-json>)`: Fügt mehrere Dokumente der Collection hinzu. Der Parameter muss ein Array sein.

**Beispiel für insertOne**

```sql
insert into inventory (sizeuom, status, quantity) values ('in', 'P', 20)
```

```shell
db.inventory.insertOne({'sizeuom': 'in', 'status': 'P', 'quantity: 20'})
```

**Beispiel für insertMany**

In SQL müssten sie einfach mehrere Insert-Statements schreiben. Dies können sie in MongoDB, aber mit einem einzigen Befehl:

```shell
db.inventory.insertOne([{'sizeuom': 'in', 'status': 'P', 'quantity: 20'},{'sizeuom': 'px', 'status': 'A', 'quantity: 50'}])
```

Beachten sie, dass wir hier ein Array übergeben mit zwei Objekten drin.

**Variablen und das Feld _id** 

Wir haben weiter oben erklärt, dass immer ein Feld *_id* bestehen muss. Wenn wir keines erstellen, wird automatisch eins erstellt. Der Datentyp des Feldes ist *ObjectId*. Im folgenden Beispiel werden wir das Feld explizit mit hinzufügen.

```shell
db.inventory.insertOne({'_id': new ObjectId(), 'sizeuom': 'in', 'status': 'P', 'quantity: 20'})
```

In MongoDB können sie (wie in relationalen Datenbanken) eine Referenz auf andere Collections hinzufügen und so die Beziehungen darstellen. Dies bedeutet, dass sie die gleiche ObjectId zweimal benötigen. Einmal in der Haupt-Collection und andererseits in der Collection, die auf die Haupt-Collection refernziert. Sie möchte die ObjectId daher gerne in einer Variablen speichern. Dies ist möglich in MongoDB Shell.

```javascript
var authorId = new ObjectId();

db.author.insertOne({'_id': authorId, 'name': 'Joe'});

db.book.insertOne({'_id': new ObjectId(), 'title': 'The book about MongoDB', 'authorId': authorId});
```

So können sie komplette Skripte schreiben, in denen sie die IDs der Objekte als Variablen behalten und später wiederverwenden.

Sie können auch Daten, die sie auslesen via *find* und *findOne* direkt in Variablen speichern. So können sie entweder auf Felder von Objekte zugreifen oder spezifische Elemente von Arrays/Listen (z.B. Suchresultate). 

### Delete

- `db.<collection>.deleteOne(<filter-json>)`: Löscht das **erste** gefundene Dokument. Optimalerweise arbeiten sie mit der *_id* als Filterung.
- `db.<collection>.deleteMany(<filter-json>)`: Löscht mehrere Dokumente. 

**Filterung**

Die Filterung funktioniert genau gleich wie bei den *find*-Befehlen. Diese werden wir hier nicht wiederholen.

### Update

- `db.<collection>.updateOne(<filter-json>, <update-json>)`: Aktualisiert das **erste** gefundene Dokument. Optimalerweise arbeiten sie mit der *_id* als Filterung
- `db.<collection>.updateMany(<filter-json>, <update-json>)`: Aktualisiert mehrere Dokumente. 
- `db.<collection>.replaceOne(<filter-json>, <update-json>)`. Ersetzt ein (Teil eines) Dokument.

**Filterung**

Die Filterung funktioniert genau gleich wie bei den *find*-Befehlen. Diese werden wir hier nicht wiederholen.

**Aktualisierung**

Der zweite Parameter beiden genannten Methoden, dient dazu zu spezifizieren, welche Felder aktualisiert werden (in den gefilterten Dokumenten). Wir beschäftigen uns folgend nur mit den zwei Befehlen *$set* und *$push*, welche viele Fälle abdecken

Stellen sie sich folgende Collection vor, welche als Basis für unsere Beispiele dient.

```json
[
{
	'_id': ObjectId("63529e2f69023f0c410ff576"),
	'name': 'Haus 1',
	'typ': 'Einfamilienhaus'
	'zimmer': [
		{'name': 'Kinderzimmer 1'},
		{'name': 'Elternzimmer'},
		{'name': 'Kinderzimmer 2'},
		{'name': 'Wohnzimmer'}
	],
	'adresse': {'strasse': 'Stationsstrasse 1', 'plz': 8000}
},
{
	'_id': ObjectId("63529e2f69023f0c410ff576"),
	'name': 'Haus 2',
	'typ': 'Einfamilienhaus'
	'zimmer': [
		{'name': 'Schlafzimmer 1'},
		{'name': 'Elternzimmer'},
		{'name': 'Kinderzimmer 2'},
		{'name': 'Büro'},
		{'name': 'Wohnzimmer'}
	],
	'adresse': {'strasse': 'Bahnhofstrasse 22', 'plz': 3000}
}
]
```

Mit dem **$set**-Schlüsselwort setzen sie ein Feld auf einen spezifischen Wert. Sie **überschreiben** den aktuellen Wert, falls einer bereits existiert.

```javascript
db.haeuser.updateOne({'name': 'Haus 1'}, {$set: {'typ': 'Reiheneckhaus', 'adresse.plz': 3001, 'zimmer.3.name': 'Gästezimmer'}})
```

Mit diesem Befehl aktualisieren sie drei Werte

- Der Typ des Hauses
- Die Postleitzahl der Adresse. Dies ist ein verschachteltes Objekt und daher muss mit der Punktnotation gearbeitet werden.
- Den Namen des 4. Zimmers. Achtung die Arrays&/Listen beginnen immer mit dem Index 0. Sie greifen wieder mit der Punktnotation auf das Objekt zu und definieren in der Liste welches Element sie bearbeiten.

Die Parameter für den Befehl *updateMany* sind gleich, mit dem Unterschied, dass möglicherweise mehrere gefundene Objekte aktualisiert werden. Da wir für unser Beispiel ein verschachteltes Objekt verwendet haben, gibt es kein Pendant in der SQL-Sprache. 

Mit dem **$push**-Schlüsselwort fügen sie ein Objekt oder einen Wert einer Liste/Array hinzu. Dies funktioniert nur, wenn das definierte Ziel ein Array ist.

```javascript
db.haeuser.updateMany({}, {$push: {"zimmer": {"name": "Küche", "hatFenster":true}}})
```

Dieser Befehl fügt allen Dokumente (da kein Filter verwendet wird), eine Küche hinzu mit einem neuen Feld.

## Optional: Aggregationen und Erweiterte Abfragen

### Aggregationen

Mit [Aggregationen](https://www.mongodb.com/docs/manual/core/aggregation-pipeline/) können komplexere Abfragen gestaltet werden. Schauen sie sich  folgendes SQL-Query an, welches mit den bisherigen MongoDB-Befehlen nicht ausgeführt werden kann.

~~~sql
select name, sum(quantity) as totalQuantity from orders where size = 'medium' group by name
~~~

Mit Aggregation können hintereinander Befehle ausgeführt werden, wobei der Output des eines Befehles dem nächsten übergeben wird (Pipeline). Die einzelnen Befehle werden hier *Stages* genannt und die Verknüpfung der *Stages* ergibt eine *Pipeline*.

Das SQL-Beispiel wird wie folgt dargestellt

~~~shell
db.orders.aggregate( [
   // Stage 1: Filter
   {
      $match: { size: "medium" }
   },
   // Stage 2: Group remaining documents by name and calculate total quantity
   {
      $group: { _id: "$name", totalQuantity: { $sum: "$quantity" } }
   }
] )
~~~

Die Schlüsselwörter *$match* und *$group* geben dabei an um welche Art von *Stage* es sich handelt. Die Stages können sich dabei auch wiederholen, z.B. 

~~~shell
db.orders.aggregate( [
   // Stage 1: Filter
   {
      $match: { size: "medium" }
   },
   // Stage 2: Group remaining documents by name and calculate total quantity
   {
      $group: { _id: "$name", totalQuantity: { $sum: "$quantity" } }
   },
   // Stage 3: Filter again
   {
      $match: { totalQuantity: { "$gte": 20 } }
   },
   // Stage 4: Sort
   {
       $sort: { totalQuantity: -1 }
   }
] )

~~~

**Vergleich zu SQL**

Auf der MongoDB-Seite findet man einen [Vergleich zwischen den SQL Begriffen und MongoDB Aggregations-Operationen](https://www.mongodb.com/docs/manual/reference/sql-aggregation-comparison/).

| SQL Begriff / Konzept / Funktion | MongoDB Aggregation Operation |
| -------------------------------- | ----------------------------- |
| WHERE                            | $match                        |
| GROUP BY                         | $group                        |
| HAVING                           | $match                        |
| SELECT                           | $project                      |
| ORDER BY                         | $sort                         |
| LIMIT                            | $limit                        |
| SUM()                            | $sum                          |
| COUNT()                          | $count                        |
| join                             | $lookup                       |

### Erweiterte Abfragen

Die bisher angeschauten Abfragen sind eher einfach. Es existieren natürlich auch Konzepte, um kompliziertere Abfragen abzusetzen, zum Beispiel:

- [Abfragen von verschachtelten Dokumenten](https://www.mongodb.com/docs/manual/tutorial/query-embedded-documents/)
- [Abfragen von Arrays](https://www.mongodb.com/docs/manual/tutorial/query-arrays/)
- [Null-Werte und fehlende Felder](https://www.mongodb.com/docs/manual/tutorial/query-for-null-fields/)

Lesen sie sich auf den entsprechenden Seiten in die Konzepte ein.

### Lookups (joins)

Obwohl wir in MongoDB keine expliziten Fremdschlüssel-Beziehungen einfügen können, gibt es den *Stage* [*$lookup*](https://www.mongodb.com/docs/manual/reference/operator/aggregation/lookup/#mongodb-pipeline-pipe.-lookup) bei in der Aggregationen-Pipeline, die einen *join* ausführen kann. Lose Beziehungen mit Referenzen existieren oft ja trotzdem. In der einfachsten Form, ist die Syntax für ein [*$lookup*](https://www.mongodb.com/docs/manual/reference/operator/aggregation/lookup/#mongodb-pipeline-pipe.-lookup) wie folgt

~~~shell
{
   $lookup:
     {
       from: <collection to join>,
       localField: <field from the input documents>,
       foreignField: <field from the documents of the "from" collection>,
       as: <output array field>
     }
}
~~~


------
Letzte Aktualisierung: 21.10.2022, Yves Nussle