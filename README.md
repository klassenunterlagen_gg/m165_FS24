![tbz_logo](./x_gitresources/tbz_logo.png)

# m165 - NoSql Datenbanken



## Einführung

[Aufträge](./Einführung/Aufträge/)

## Datenmodellierung

[Theorie](./Datenmodell/Readme.md)

[Aufträge](./Datenmodell/Aufträge/)

## Datenmanipulation

[ACID / CAP / BASE](./Datenmanipulation/ACID_CAP_BASE.md)

[MongoDB Übersicht](./MongoDb)

[Software](./Software.md)

[Aufträge](./Datenmanipulation/Aufträge/)



------

Letzte Aktualisierung: 05.09.2022, Yves Nussle

