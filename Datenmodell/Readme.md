![tbz_logo](../x_gitresources/tbz_logo.png)

# Datenmodellierung

[TOC]

## Wissen aus relationalen Datenbanken

In **relationalen Datenbanken** haben sie [verschiedene Konzepte](https://gitlab.com/ch-tbz-it/Stud/m162/-/blob/main/Datenmodellierung/Theorie_Datenmodellierung.md) gelernt. Die wichtigsten:

- Entitäten, Kardinalitäten und Beziehungen
- Datenmodell als Sammlung und Darstellung
- Referenzielle Integrität
- Normalisierung
- Vermeidung von Redundanzen.

Diese Konzepte sind für alle relationalen Modelle und Datenbanken anwendbar, wobei lediglich das  physische Datenmodell produktspezifisch ist. 

**Konzeptionelles ERM aus der relationalen Datenbank "School"**

![ModelSchoolManagement-Konzept](./x_gitresources/ModelSchoolManagement-Konzept.png)

**Logisches ERM aus der relationalen Datenbank "School"**

![ModelSchoolManagement-Konzept](./x_gitresources/ModelSchoolManagement-Logisch.png)

## Grundsätzliche Überlegungen

Da wir grundsätzlich von vier verschiedenen Familien von NoSql Datenbank-Typen sprechen, wird es gleich klar, dass es kein einheitliches Konzept zur Datenmodellierung geben kann, welches universell gültig ist.

Ein konzeptionelles Modell (wie das obige) ist nicht Datenbanktyp abhängig und **universell gültig**. Sie können auf diese Art auch NoSql-Datenbanktypen konzipieren. Die Umsetzung in das logische Schema variiert dann aber stark.

## Logisches Datenmodell in MongoDB

In MongoDB (und wahrscheinlich den meisten Document-Datenbanken) lassen sich logische Modelle genau gleich umsetzen wie bei relationalen Datenbanken (also normalisiert und redundanzfrei).  Dies ist aber nicht unbedingt sinnvoll, da die Stärke von Document-Datenbanken ignoriert werden. In Document-Datenbanken sind **Verschachtelungen von Daten** möglich. 

Obwohl wir das ERM direkt aus der relationalen Datenbank übernehmen könnten, macht es Sinn dies zu überdenken und unsere Applikation in die Modellierung mit einzubeziehen. Folgend ein Beispiel bei dem verschachtelte Objekte verwendet werden. Das Dokument könnte hierbei im XML- oder JSON-Format gespeichert sein und tatsächlich verwendet MongoDB das JSON-Format. 

![DocumentStore-Model1](./x_gitresources/DocumentStore-Model1.png)

Es gilt sowohl Vorteile als auch Nachteile eines Modelles abzuwägen. Ein **Vorteil** im Model oben ist, dass eine Klasse inklusive aller Studenten mit einer Abfrage ausgelesen werden können. Ein **Nachteil** im Model oben ist, dass das Auslesen **aller** Studenten teuer (im Sinne von Rechenleistung) ist. Die Frage ist also wie oft sie welche Art von Abfragen stellen und so entscheiden welches Datenmodell sinnvoll ist.

Ein weiteres Beispiel, bei die Verschachtelung noch weiter getrieben wird. **Dieses Model ist nicht sinnvoll**. Sie können zwar alle Daten mit einer Abfrage auslesen, aber sie müssen **Redundanzen** einführen. Jede Lehrperson, jedes Modul ist mehrfach gespeichert. 

![    ](./x_gitresources/DocumentStore-Model2.png)

### Referenzielle Integrität

Referenzielle Integrität wird nicht gewährleistet. Die Applikation ist Zuständig, dass die Daten mit der notwendigen Integrität gespeichert werden. 

![RefInteg](./x_gitresources/RefInteg.png)

Im folgenden Beispiel können sie jederzeit dem Wert *Document1Id* einen Wert zuweisen, welcher im Dokument *Document1* nicht als *ID* existiert. Die Datenbank stellt keine Überprüfung an, obwohl im Schema so modelliert.

**Document1:**

~~~ json
[{
	ID: 1,
    Description: "Document with ID 1",
    Other: {...}
},{
	ID: 2,
    Description: "Document with ID 2",
    Other: {...}
}]
~~~

**Document2:**

~~~ json
[{
	ID: 100,
    Document1ID: 55,
    Value:0
}]
~~~

### Denormalisierung und Redundanzen

Auf Normalisierung wird in NoSql Datenbanken oft explizit verzichtet. Schauen sie sich den folgenden Vergleich aus einem relationalen Datenmodell (links) und einem möglichen Datenmodell für einen Document-Store (rechts).![Normalisierung](./x_gitresources/Normalisierung.png)

In der Document-Store Datenbank würde man wahrscheinlich darauf verzichten, die Adresse als eigene Collection zu speichern, da die Adresse selten bis nie alleine ausgelesen wird, sondern immer im Zusammenhang mit einer Person - entweder Student oder Teacher. Es kann tatsächlich vorkommen, dass so eine Adresse zweimal erfasst wird. Dies wird aber selten der Fall sein. 

Ihr Datenmodell hängt natürlich stark vom Anwendungsfalls ab. Wenn sie eine Applikation erstellen deren Hauptanwendung die Verwaltung von Adressdaten ist (z.B. Adressbuch, GPS, etc), würden sie keine Redundanzen einfügen wollen.

**Die grössere Performance muss den Problemen die Redundanzen bringen überwiegen.**

## JSON Schema

Bisher wurde verschachtelte Rechtecke verwendet, um die Verschachtelung von JSON-Objekten darzustellen. Tatsächlich gibt es kein Standard eine NoSql-Datenbank zu visualisieren. 

Da MongoDB JSON als Datenbank-Dokument verwendet, können wir aber auf JSON-Schemas ausweichen und die Datenbank korrekt darzustellen. Mit Hilfe von JSON-Schemas können wir einen Dokument-Typ einer Collection spezifizieren und auch sogar validieren.

**Ein JSON-Schema ist ein JSON-Dokument, welches die Semantik eines  anderen JSON-Dokuments beschreibt**.

Sie finden unter *Quellen* zwei Links, die ihnen Informationen liefern.

**Wichtig**: Es gibt grundsätzlich **eine JSON-Schema-Datei pro Dokumenttyp**.

**Beispiel**: Folgendes JSON-Dokument repräsentiert eine Person und ihre Adresse. Anschliessend wird das JSON-Schema dazu gezeigt, welches definiert was der Datentyp der einzelnen Felder ist.

```json
{
	"name": "Joe",
	"address": {
		"street": "Stationsstrasse 12",
		"postcode": 8000,
		"city": "Zürich"
	}
}
```
Das folgende Dokument enthält die Definitionen für die Felder.

```json
{
	"$id": "<TBD>",
	"$schema": "https://json-schema.org/draft/2020-12/schema",
	"description": "describes a student",
	"type": "object",
	"properties": {
		"name": {
			"type": "string",
			"description": "The name of the student"
		},
		"address": {
			"type": "object",
			"properties": {
				"street": { "type": "string", "description": "The street + No."},
				"postcode": { "type": "float", "description": "The postcode" },
				"city": { "type": "string", "description": "The city/town" }
			}
		}
	}
}
```

Ein paar wichtige Punkte:

- Das Feld "$id" enthält normalerweise die URL unter welcher dieses Schema publiziert ist. 
- Das Feld "$schema" enthält die Version dieses JSON-Schemas. Es kann sein, dass die sich die Begriffe über die Zeit ändern. Darum ist es wichtig, die Version zu definieren.
- Weitere Informationen finden sie in den Links *unter Quellen*.

## Modellierungstechniken

Wie erhalten wir das logische Schema für MongoDB aus einem konzeptionellen Modell? Grundsätzlich gilt es die drei Typen von Beziehungen abzubilden, die im folgenden Modell gezeigt werden. 

![Shop-Example-Theorie-Konzeptionell](./x_gitresources/Shop-Example-Theorie-Konzeptionell.png)

**One-To-One Beziehungen**

In diese Kategorie gehören alle Beziehungen mit 1/C : 1/C  - also 1:C, 1:1, C:1, C:C. Sie können jeweils als verschachtelte Dokumente eingefügt werden. Theoretisch können beide Entitäten als Hauptdokument dienen. Wichtig ist also wieder zu verstehen wie die Applikation die Daten liest. In unserem Beispiel wird der *User* zum Hauptdokument und enthält einen Warenkorb.

```json
{
	"$id": "<TBD>",
	"$schema": "https://json-schema.org/draft/2020-12/schema",
	"description": "1-1 Beziehungsbeispiel. User-Objekt",
	"type": "object",
	"properties": {
		"name": {
			"type": "string",
			"description": "Der Name des Benutzers"
		},
		"warenkorb": {
			"type": "object",
			"properties": {
				"aktualisierungsDatum": { "type": "string", "description": "Das Datum wann das letzte mal aktualisiert wurde"},
			}
		}
	}
}
```

**One-To-Many Beziehungen**

In diese Kategorie gehören alle Beziehungen mit 1/C : M(C) - also 1:M, 1:MC, C:M, C:MC. Sie können ebenfalls als verschachtelte Dokumente abgebildet werden. In diesem Fall scheint klar, welches das Hauptdokument sein muss - die Entität mit der *Many*-Beziehung.

```json
{
	"$id": "<TBD>",
	"$schema": "https://json-schema.org/draft/2020-12/schema",
	"description": "1-Many Beziehungsbeispiel. Artikel-Objekt",
	"type": "object",
	"properties": {
		"name": { "type": "string", "description": "Der Name des Artikels"},
		"preis": { "type": "number", "description": "Der Preis des Artikels"},
		"farbe": {
			"type": "object",
			"properties": {
				"r": { "type": "string", "description": "Hex-Farbwert für Rot"},
				"g": { "type": "string", "description": "Hex-Farbwert für Grün"},
				"b": { "type": "string", "description": "Hex-Farbwert für Blau"}
			}
		}
	}
}
```

**Many-To-Many Beziehungen**

In diese Kategorie gehören alle Beziehungen mit M(C) : M(C) - also M:M, M:MC, MC:M, MC:MC. Bei diesen Beziehungstypen muss weiterhin mit Referenzen (Fremdschlüssel) gearbeitet werden. Auch hier können die Referenzen theoretisch in beiden Entitäten gesetzt werden und es gilt die Applikation und Abfragestruktur zu beachten. Im Hauptdokument wird ein Array erstellt - entweder mit einfachen Datentypen oder Objekten.

```json
{
	"$id": "<TBD>",
	"$schema": "https://json-schema.org/draft/2020-12/schema",
	"description": "Many-Many Beziehungsbeispiel. Warenkorb-Objekt",
	"type": "object",
	"properties": {
		"aktualisierungsDatum": { "type": "string", "description": "Das Datum wann das letzte mal aktualisiert wurde"},
		"warenkorbartikel": {
			"type": "array",
			"items": {
				"type": "object",
				"properties": {
					"artikelId": { "type": "<ObjectId>", "description": "ID des Artikels"},
					"menge": { "type": "number", "description": "Menge des Artikels"}
				}
			}
		}
	}
}
```

**Beispiel Schema**

Unsere Beispiele oben ergeben nun also folgendes Bild und JSON-Schema.

![Shop-Example-Theorie-Logisch](./x_gitresources/Shop-Example-Theorie-Logisch.png)

Im folgenden Gesamt-Beispiel wurde zusätzlich noch die *_id* der Dokumente hinzugefügt. Erst diese *_id* ermöglicht die Referenzierung. Jedes Dokument in MongoDB muss dieses Feld besitzen. Dazu mehr, wenn wir mit Abfragen arbeiten.

```json
{
	"$id": "<TBD>",
	"$schema": "https://json-schema.org/draft/2020-12/schema",
	"description": "Gesamtbeispiel Artikel",
	"type": "object",
	"properties": {
		"_id": { "type": "<ObjectId>", "description": "Die ID des Artikels"},
		"name": { "type": "string", "description": "Der Name des Artikels"},
		"preis": { "type": "number", "description": "Der Preis des Artikels"},
		"farbe": {
			"type": "object",
			"properties": {
				"r": { "type": "string", "description": "Hex-Farbwert für Rot"},
				"g": { "type": "string", "description": "Hex-Farbwert für Grün"},
				"b": { "type": "string", "description": "Hex-Farbwert für Blau"}
			}
		}
	}
}

{
	"$id": "<TBD>",
	"$schema": "https://json-schema.org/draft/2020-12/schema",
	"description": "Gesamtbeispiel Benutzer",
	"type": "object",
	"properties": {
		"_id": { "type": "<ObjectId>", "description": "Die ID des Benutzers"},
		"name": { "type": "string", "description": "Der Name des Benutzers"},
		"warenkorb": {
			"type": "object",
			"properties": {
				"aktualisierungsDatum": { "type": "string", "description": "Das Datum wann das letzte mal aktualisiert wurde"},
				"warenkorbartikel": {
					"type": "array",
					"items": {
						"type": "object",
						"properties": {
							"artikelId": { "type": "<ObjectId>", "description": "ID des Artikels"},
							"menge": { "type": "number", "description": "Menge des Artikels"}
						}
					}
				}
			}
		}
	}
}
```

## Quellen

- <https://json-schema.org/learn/>. Wahrscheinlich reicht es, wenn sie die "Miscellaneous Examples"  und "Example Collection" anschauen.
- <https://www.mongodb.com/docs/manual/core/data-modeling-introduction/>. Referenz auf die Datenmodellierung in MongoDB.
- <https://www.mongodb.com/basics/json-schema-examples>
- <https://phoenixnap.com/kb/nosql-data-modeling>

## Übungen

Die folgenden Übungen sind freiwillig und dienen ihnen zum Vertiefen. Mindestens **die erste Übung ist zu empfehlen**, so dass sie sich in JSON-Schemas einarbeiten.

- [JSON Schema I](./Übungen/JSONSchema_I.md).  Sie üben den Umgang mit JSON-Schemas
- [School I](./Übungen/School_I.md). Sie üben das erstellen eines JSON-Schemas aufgrund eines relationalen Datenbankmodells
- [Datenmodell I](./Übungen/Datenmodell_I.md). Sie üben das Erstellen eines Models aufgrund einer Beschreibung
- [Datenmodell II](./Übungen/Datenmodell_II.md). Sie üben das Denormalisieren ihres Modells.
- [School II](./Übungen/School_II.md). Sie üben das Denormalisieren eines relationalen Datenbankmodells



------

 Letzte Aktualisierung: 21.10.2022, Yves Nussle