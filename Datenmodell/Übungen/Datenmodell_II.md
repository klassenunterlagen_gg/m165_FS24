![tbz_logo](../../x_gitresources/tbz_logo.png)

# Datenbank Modell II

Sie gestalten nun das JSON-Schema um, so dass verschachtelte Objekte Verwendung finden. Welche Verschachtelung ergibt hier Sinn? Die Regeln aus dem ersten Auftrag sind nach wie vor gültig, aber nun sind sie frei wie sie ihre Collections/Dokumente gestalten.

Erstellen sie ein Datenmodell als JSON-Schema. Sie können dazu pro Collection ein JSON-Schema erstellen oder einfach alle Dokument-Typen in einem einzigen JSON-Schema. Verwenden sie typische (z.B. Name) und notwendige Felder für die verschiedenen Dokumente.

**Zeit**: ~ 20-40 Minuten

------

Letzte Aktualisierung: 21.10.2022, Yves Nussle