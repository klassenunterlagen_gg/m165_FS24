![tbz_logo](../../x_gitresources/tbz_logo.png)

# Schule / Demo Projekt II

Verändern sie das JSON-Schema, welches sie für das Demo-Projekt erstellt haben und verwenden sie Verschachtelungen:

- Überlegen sie sich gut, wo Verschachtelungen Sinn ergeben. Welche Daten werden üblicherweise zusammen ausgelesen?
- Entfernen sie die Transformationstabellen. Wie können sie diese auflösen? 
- Fügen sie Adressdaten hinzu für Lernende und Lehrpersonen
- Fügen sie den Unterrichtstag (z. B. MO, DI, etc) und die Startzeit hinzu (z.B. 8:10 oder 13:00)

**Zeit**: ~20-40 Minuten

------

Letzte Aktualisierung: 21.10.2022, Yves Nussle