![tbz_logo](../../x_gitresources/tbz_logo.png)

# JSON-Schema I

### a) Einfaches Schema

Erstellen sie ein JSON-Schema für die die Entität *Buch*. Ein *Buch* hat dabei folgende Eigenschaften:

- Inhalt. Dies ist der gesamte Text eines Buches
- Anzahl Seiten. 
- Autor
- Titel

Verwenden sie jeweils das "description"-Feld um die Inhalte korrekt zu beschreiben. Schauen sie sich [die Beispiel](https://json-schema.org/learn/) an, um den Aufbau zu verstehen.

**Zeit**: ~15-30 Minuten



### b) Erweitertes Schema

Das *Buch* wird nun anders dargestellt. Erstellen sie ein neues JSON-Schema, welches die folgenden Eigenschaften abbildet:

- Autor. Dies ist ein Objekt mit den Felder *Vorname*, *Nachname*
- Titel
- Seiten. Dies ist ein Array und enthält die Seitennummer und den Text dieser Seite.

Verwenden sie jeweils das "description"-Feld um die Inhalte korrekt zu beschreiben. Schauen sie sich [die erweiterten Beispiel](https://json-schema.org/learn/miscellaneous-examples.html) an, um zu verstehen wie sie mit Arrays und Referenzen umgehen.

**Zeit**: 15-30 Minuten

------

Letzte Aktualisierung: 21.10.2022, Yves Nussle