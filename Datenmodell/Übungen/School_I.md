![tbz_logo](../../x_gitresources/tbz_logo.png)

# Schule / Demo Projekt I

Im Demo-Projekt haben sie folgendes ERM kennengelernt. 

![ModelSchoolManagement-Logisch](../x_gitresources/ModelSchoolManagement-Logisch.png)

Bilden sie dieses ERM als JSON-Schema ab - ohne Änderungen zu erstellen. Pro Entität des ERM soll ein JSON Objekt erstellt werden. Sie können dazu pro Collection ein JSON-Schema erstellen oder einfach alle Dokument-Typen in einem einzigen JSON-Schema. 

**Zeit**: ~20-60 Minuten

------

Letzte Aktualisierung: 21.10.2022, Yves Nussle