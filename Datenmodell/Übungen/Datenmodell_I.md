![tbz_logo](../../x_gitresources/tbz_logo.png)

# Datenbank Modell I

Sie kriegen von einem Kunden einen Auftrag ein Modell für eine Social Media Plattform zu erstellen, die die Funktionen von Twitter abbilden, im Spezifischen:

- Es gibt Benutzer
- Ein Benutzer kann anderen folgen (und kann also auch gefolgt werden)
- Ein Benutzer kann einen Post erfassen.
- Ein Post kann kein oder mehrere Bilder enthalten.
- Ein Benutzer kann auf einen Post kommentieren.
- Ein Benutzer kann auf einen Kommentar kommentieren.

Der Kunde erwartet von ihnen, dass folgende Collections existieren: **User, Post, Comment, Image**. **Halten sie diese Collections ein.**

Die Posts müssen in der korrekten Reihenfolge erscheinen und benötigen daher ein Erstellungsdatum.

Erstellen sie ein Datenmodell als JSON-Schema. Sie können dazu pro Collection ein JSON-Schema erstellen oder einfach alle Dokument-Typen in einem einzigen JSON-Schema. Verwenden sie typische (z.B. Name) und notwendige Felder für die verschiedenen Dokumente.

**Zeit**: 30-60 Minuten

------

Letzte Aktualisierung: 21.10.2022, Yves Nussle