![tbz_logo](../../x_gitresources/tbz_logo.png)

# Gruppenpuzzle

[TOC]

### Stammgruppe

Von ihrer Lehrperson werden sie in Gruppen eingeteilt von vier Personen. In dieser Gruppe verteilen sie die folgenden vier Themen auf sich auf:

- NoSql Datenbank: Column
- NoSql Datenbank: Document-Store
- NoSql Datenbank: Graph
- NoSql Datenbank: Key-Value-Store

**Zeit**: 10'

### Expertengruppe

Nun treffen sich alle Personen mit dem gleichen Thema zusammen. In dieser Gruppe erarbeiten sie sich Expertenwissen über ihr gewähltes Thema.

Erstellen sie ein **Dokument** (ein Handout für die anderen Gruppen) in welchem sie folgende Themen bearbeiten. Strukturieren sie ihr Dokument mit einem Inhaltsverzeichnis, so dass sich ihre Kollegen dann auch zurechtfinden.

- Lesen sie sich in das den zugewiesenen NoSql Datenbank Typ ein. Sie finden dazu weiter unten entsprechende Links. Suchen sie auch nach weitere Ressourcen und Erklärungen im Internet.
- Erklärung: Erklären sie den NoSql Datenbank Typ den sie zugewiesen bekamen in **eigenen Worten**
- Definieren sie Anwendungsfälle (Use Cases) in welchen dieser Typ Sinn ergibt.
- Erstellen sie ein **eigenes Beispiel**  und visualisieren es. Kopieren sie nicht einfach ein Beispiel welches sie gefunden haben.
- Halten sie fest, welches die **Unterschiede** zu Relationalen Datenbanken sind.
- Wie steht es um die Performance, spezifische **Schreiben und Lesen**? Welche anderen Vor- und Nachteile hat ihr Datenbank Typ?
- Welche Untertypen gibt es?
- Welches sind die bekanntesten Produkte?

**Zeit**: 40'

### Stammgruppe

Nun kehren sie zurück in ihre Stammgruppe. Jedes Gruppenmitglied erklärt nun sein NoSql Datenbank Typ den anderen Mitgliedern.

**Zeit**: 45'



### NoSql Datenbank Typen

**Column (Family)**

https://scaleyourapp.com/wide-column-and-column-oriented-databases/

**Document-Store** 

https://www.kdnuggets.com/2021/03/understanding-nosql-database-types-document.html

https://www.techtarget.com/searchdatamanagement/tip/NoSQL-database-types-explained-Document-based-databases

**Key-Value Store**

https://www.techtarget.com/searchdatamanagement/tip/NoSQL-database-types-explained-Key-value-store

**Graph**

https://12ft.io/proxy?q=https%3A%2F%2Fwww.techtarget.com%2Fsearchdatamanagement%2Ftip%2FNoSQL-database-types-explained-Graph



------

Letzte Aktualisierung: 30.07.2022, Yves Nussle