![tbz_logo](../x_gitresources/tbz_logo.png)

# Projektauftrag

[TOC]

Sie definieren eine Datenbank (z.B. Webshop, Social Media, U-Bahn-Karte, etc), die sie als Projekt umsetzen. **Wählen sie ihr Projekt zusammen mit dem Datenbank-Typ aus, so dass die Kombination passend ist.**

## Datenbank-Familie

Wir haben im Unterricht 4 Datenbank-Familien kennengelernt. Bereits ausführlich behandelt haben wir die Document-Stores mit MongoDB. Die Key-Value-Datenbanken scheinen relativ simple und daher sollen sie eine der beiden Datenbank-Familien verwenden:

- (Wide-)Column Datenbanken (z.B. Cassandra)
- Graph Datenbanken (z.B. Neo4J)

Wählen sie die Datenbank-Familie zusammen mit ihrem Projekt aus, so dass die Kombination passend ist.

## Zu behandelnde Themen

Sie werden die gleichen Inhalte behandeln wie wir sie bereits mit MongoDB behandelt hatten - nun einfach angewandt auf eine andere Datenbank-Familie.

#### Dokumentation

Sie erstellen eine Dokumentation mit folgenden Inhalten:

- Beschreibung des Projekts und welchen Datenbank-Typ sie verwenden (3-5 Sätze)
- Bearbeitung und Dokumentation der folgenden Themen. Inhalte, die nicht mit einem Skript abgedeckt werden.
- Fazit, Reflektion. 
- Quellen. Alle Inhalte, die nicht von ihnen sind, müssen mit einer Quelle belegt sein.

#### Skript(s)

Sie erstellen Skripts mit den Anweisungen für die Datenbank. Mögliche Inhalte sind (abhängig vom Projekt und Datenbanktyp):

- Skript für das Modell
- Skript für die Installation
- Skript für die Rechte-Verwaltung
- Skript für die Inserts
- Skript für die 

#### Installation

Sie installieren ihre Datenbank auf dem AWS Server. In der Dokumentation fügen sie notwendige Inhalte (z.b. Links und Beschreibung der Schritte) hinzu. 

Sie erstellen zusätzlich ein **Skript** für die Installation - wahrscheinlich ein Bash-Skript - nach Anleitung auf der Webseite. Die Inhalte des Skripts gehört nicht in die Dokumentation.

Sie können die [Installationsskripts](https://gitlab.com/ch-tbz-it/Stud/m165/m165_scripts/-/tree/main/scripts) für MongoDB und MariaDB anschauen.

#### Datenmodellierung

Sie modellieren ihr Projekt mit den entsprechenden Tools, z.B. Diagramme oder andere Beschreibungsformen - abhängig von dem Datenbanktyp. 

Cassandra: <https://cassandra.apache.org/doc/latest/cassandra/data_modeling/index.html>

Neo4J: <https://neo4j.com/developer/data-modeling/>

#### Rechte und Rollen

In der Dokumentation erklären und begründen Sie ihre Entscheidung welche Benutzer, Rechte und Rollen eingesetzt werden sollen.

Sie erstellen ein Skript um die Benutzer (und möglicherweise Rechte/Rollen) zu erstellen. 

#### Abfragen und Abfragesprache

Sie erstellen ein Skript um die Datenbank zu befüllen. 

Sie erstellen ein Skript, um Daten zu lesen, zu aktualisieren und zu löschen. Das Skript soll zeigen, dass sie die Befehle verstehen und anwenden können. Mit entsprechend komplizierteren Abfragen/Befehlen können sie zusätzliche Punkte holen.

In der Dokumentation erklären sie was ihre Abfragen tun, aber ohne die Abfragen aufzuführen. Vermeiden sie redundante Informationen in der Dokumentation und den Skripten.

#### Backup und Restore

Sie erstellen ein Skript welches ein Backup und Restore ausführt.

Ergänzen Sie in der Dokumentation das Skript und führen aus was genau passiert.

#### Skalierung

Erklären Sie in der Dokumentation wie sie die Datenbank skalieren können.  Sie dürfen hier gerne einen Bezug zu dem CAP-Theorem herstellen.




------
Letzte Aktualisierung: 28.09.2022, Yves Nussle