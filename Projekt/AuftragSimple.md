![tbz_logo](../x_gitresources/tbz_logo.png)

# Projektauftrag

[TOC]

Sie definieren eine Datenbank (z.B. Webshop, Social Media, U-Bahn-Karte, etc), die sie als Projekt umsetzen. **Wählen sie ihr Projekt zusammen mit dem Datenbank-Typ aus, so dass die Kombination passend ist.**

## Datenbank-Familie

Wir haben im Unterricht 4 Datenbank-Familien kennengelernt. Bereits ausführlich behandelt haben wir die Document-Stores mit MongoDB. Die Key-Value-Datenbanken scheinen relativ simple und daher sollen sie eine der beiden Datenbank-Familien verwenden:

- (Wide-)Column Datenbanken (z.B. Cassandra)
- Graph Datenbanken (z.B. Neo4J)

Wählen sie die Datenbank-Familie zusammen mit ihrem Projekt aus, so dass die Kombination passend ist.

## Zu behandelnde Themen

Sie werden die gleichen Inhalte behandeln wie wir sie bereits mit MongoDB behandelt hatten - nun einfach angewandt auf eine andere Datenbank-Familie.

#### Dokumentation

Sie erstellen eine Dokumentation mit folgenden Inhalten:

- Beschreibung des Projekts und welchen Datenbank-Typ sie verwenden (3-5 Sätze). **Dies ist der erste Schritt in ihrem Projekt**
- Fazit, Reflektion. **Dies ist der letzte Schritt in ihrem Projekt**
- Quellen. Alle Inhalte, die nicht von ihnen sind, müssen mit einer Quelle belegt sein.

#### Installation

Sie installieren ihre Datenbank auf dem **bestehenden** AWS Server.

Links zu den Produkten [finden sie hier](Links.txt).

#### Datenmodellierung

Sie modellieren ihr Projekt mit den entsprechenden Tools, z.B. Diagramme oder andere Beschreibungsformen - abhängig von dem Datenbanktyp.

Links zu den Produkten [finden sie hier](Links.txt).

**Sie erstellen Diagramme, Skripte oder was sonst notwendig ist für ihr Produkt.** 

#### Abfragen und Abfragesprache

Links zu den Produkten [finden sie hier](Links.txt).

**Sie erstellen ein Skript um die Datenbank zu befüllen.** 

Sie erstellen ein Skript, um Daten zu lesen, zu aktualisieren und zu löschen. Das Skript soll zeigen, dass sie die Befehle verstehen und anwenden können. Mit entsprechend komplizierteren Abfragen/Befehlen können sie zusätzliche Punkte holen.




------
Letzte Aktualisierung: 27.10.2022, Yves Nussle